package com.ycagri.pdftrontest

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.pdftron.pdf.config.PDFViewCtrlConfig
import com.pdftron.pdf.config.ToolManagerBuilder
import com.pdftron.pdf.config.ViewerConfig
import com.pdftron.pdf.controls.DocumentActivity

class PdfTronActivity: DocumentActivity() {

    companion object {

        fun startActivity(context: Context) {
            val intent = Intent(context, PdfTronActivity::class.java)
            intent.putExtra(EXTRA_FILE_URI, Uri.parse("file:///android_asset/-LuCOIBFuhEaxqiaylv7.pdf"))
            intent.putExtra(EXTRA_CONFIG, createConfig(context, "Title"))
            context.startActivity(intent)
        }

        private fun createConfig(context: Context, title: String): ViewerConfig {
            val tmBuilder = ToolManagerBuilder.from()
                .setUseDigitalSignature(false)
                .setAutoResizeFreeText(false)

            val builder = ViewerConfig.Builder()
                .fullscreenModeEnabled(true)
                .multiTabEnabled(false)
                .documentEditingEnabled(true)
                .longPressQuickMenuEnabled(true)
                .showPageNumberIndicator(true)
                .showBottomNavBar(true)
                .showThumbnailView(true)
                .showBookmarksView(true)
                .showSearchView(true)
                .showShareOption(false)
                .showDocumentSettingsOption(true)
                .showAnnotationToolbarOption(true)
                .showOpenFileOption(true)
                .showOpenUrlOption(true)
                .showEditPagesOption(true)
                .showPrintOption(true)
                .showCloseTabOption(false)
                .showAnnotationsList(true)
                .showOutlineList(true)
                .showUserBookmarksList(true)
                .pdfViewCtrlConfig(PDFViewCtrlConfig.getDefaultConfig(context))
                .toolbarTitle(title)
                .toolManagerBuilder(tmBuilder)

            return builder.build()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}